﻿using AdoToPreparerConverter.Domain;

internal class Program
{
    private static void Main(string[] args)
    {
        var directory = "../../../Input/";
        var files = Directory.GetFiles(directory, "*.txt");
        var result = string.Empty;
        foreach (var file in files)
        {
            var input = File.ReadAllText(file);
            var statement = new AdoNetReturnStatementClass().ParseFromStatement(input);

            var converter = new StatementConverter(statement);
            result += converter.ConvertToDapper();
            result += '\n';
            
        } 
        var path = $"{directory}DocumentFactory.cs";
            File.WriteAllText(path, result);
    }
}