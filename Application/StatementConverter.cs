using System.Text;
using AdoToPreparerConverter.Domain;

public class StatementConverter
{
    private readonly AdoNetReturnStatementClass _statementClass;
    private StringBuilder _stringBuilder;
    public StatementConverter(AdoNetReturnStatementClass statementClass)
    {
        _statementClass = statementClass;
        _stringBuilder = new StringBuilder();
    }

    public string ConvertToDapper()
    {
        _stringBuilder.Append($"private {_statementClass.ReturnType} Prepare{_statementClass.ReturnType}(");
        _stringBuilder.AppendJoin(',', _statementClass.ValueNameTypeDictionary.Select(keyValue => $"{keyValue.Value} {keyValue.Key.ToLower()}").ToList());
        _stringBuilder.Append(")");
        _stringBuilder.AppendLine("{");
            _stringBuilder.AppendLine($"return _fixture.Build<{_statementClass.ReturnType}>()");
            foreach (var keyValue in _statementClass.ValueNameTypeDictionary)
            {
                _stringBuilder.AppendLine($".With(r => r.{keyValue.Key}, {keyValue.Key.ToLower()})");
            }
            _stringBuilder.AppendLine($".Create();");
        _stringBuilder.AppendLine("}");

        return _stringBuilder.ToString();
    }
    
}