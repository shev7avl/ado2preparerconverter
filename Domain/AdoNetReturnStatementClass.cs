using System.Text.RegularExpressions;

namespace AdoToPreparerConverter.Domain
{
    public class AdoNetReturnStatementClass
    {
        private string _returnStatement;
        public string ReturnType {get; private set;}

        public Dictionary<string, string> ValueNameTypeDictionary {get; private set;}
        
        public AdoNetReturnStatementClass ParseFromStatement(string returnStatement)
        {
            _returnStatement = returnStatement;
            ParseReturnType();
            ParseValues();
            return this;
        }

        private void ParseReturnType()
        {
            Match match = Regex.Match(_returnStatement, @"return new ([a-zA-Z0-9_]+)");
            if (match.Success)
            {
                ReturnType = match.Groups[1].Value;
                Console.WriteLine("Found match: {0}", match.Groups[1].Value);   
            }
        }

        private void ParseValues()
        {
            var matches = Regex.Matches(_returnStatement, @"([a-zA-Z0-9_]+) = Utility.Read<([a-zA-Z0-9_]+)>");
            if (!matches.Any())
            {
                Console.WriteLine("No matches found");
                return;
            }
            ValueNameTypeDictionary = new Dictionary<string, string>();
            foreach (Match match in matches)
            {
                ValueNameTypeDictionary.Add(match.Groups[1].Value, match.Groups[2].Value);
            }
        }
    }
}